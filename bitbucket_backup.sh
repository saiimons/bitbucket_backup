#!/bin/sh

function backup_hg {
	if [ -d "$1" ]; then
		cd $1
		hg pull
	else
		hg clone -U ssh://hg@bitbucket.org/$2 $1
	fi
}

function backup_git {
	if [ -d "$1" ]; then
		cd $1
		git pull
	else
		git clone -n https://$USER_NAME:$APP_PWD@bitbucket.org/$2 $1
	fi
}

function backup_scm {
	TYPE=$3
	REPO=$2
	REPO_BACKUP_LOCATION=$BACKUP_LOCATION/$REPO
	REPO_REMOTE_PATH=$REPO
	if [ "git" = "$1" ]; then
		REPO_REMOTE_PATH=$REPO_REMOTE_PATH.git
	fi
	if [ "repo" = "$TYPE" ]; then
		REPO_BACKUP_LOCATION=$REPO_BACKUP_LOCATION/repo
	elif [ "wiki" = "$TYPE" ]; then
		REPO_BACKUP_LOCATION=$REPO_BACKUP_LOCATION/wiki
		REPO_REMOTE_PATH=$REPO_REMOTE_PATH/wiki
	fi
	if [ "hg" = "$1" ]; then
		backup_hg $REPO_BACKUP_LOCATION $REPO_REMOTE_PATH
	elif [ "git" = "$1" ]; then
		backup_git $REPO_BACKUP_LOCATION $REPO_REMOTE_PATH
	fi
}

mkdir -p $BACKUP_LOCATION

repositories=`curl -s -S --user $USER_NAME:$APP_PWD https://api.bitbucket.org/2.0/repositories/$REPO_OWNER\?pagelen\=100 | jq -r '.values[] | "\(.full_name) \(.scm) \(.has_issues) \(.has_wiki)"'`

OIFS="$IFS"
IFS=$'\n'

echo Backing up ${#repositories[@]} repositories

for repository in $repositories
do
	repository_name=`echo $repository | cut -d ' ' -f1`
	has_issues=`echo $repository | cut -d ' ' -f3`
	has_wiki=`echo $repository | cut -d ' ' -f4`
	scm=`echo $repository | cut -d ' ' -f2`

	echo "backing up $repository_name"
	backup_scm $scm $repository_name repo

	if [ "true" = "$has_wiki" ]; then
		echo "backing up $repository_name wiki"
		backup_scm $scm $repository_name wiki
	fi

	if [ "true" = "$has_issues" ]; then
		echo "backing up $repository_name issues"
		ISSUES_BACKUP_LOCATION=$BACKUP_LOCATION/$repository_name/issues
		mkdir -p $ISSUES_BACKUP_LOCATION
		curl -s -S --user $USER_NAME:$APP_PWD https://api.bitbucket.org/1.0/repositories/$repository_name/issues > $ISSUES_BACKUP_LOCATION/issues.json
	fi
done
IFS="$OIFS"
